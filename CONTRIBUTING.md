# Welcome to Momento-Drupal Contributing Guide

This document outlines the guidelines for contributing to the Momento-Drupal project. We value the contributions of our community and aim to create a collaborative and inclusive environment.

## How to Contribute

### Reporting Issues

If you encounter a bug or have a feature request, please [open an issue](https://www.drupal.org/project/issues/momento?categories=All). When reporting issues, provide as much detail as possible, including steps to reproduce the problem, your environment, and any relevant error messages.

### Making Changes

We welcome contributions in the form of bug fixes, feature enhancements, documentation improvements, and more. To contribute code, follow these guidelines:

1. **Set Up Git Authentication for Drupal.org Projects:**
   Follow the instructions [here](https://www.drupal.org/docs/git/using-git/using-git-over-https-with-two-factor-authentication) to set up your Git authentication for Drupal.org projects.

2. **Set Up Your Drupal Environment:**
   Follow the instructions in the [Drupal Git Instructions](https://www.drupal.org/project/momento/git-instructions) to set up your Drupal environment.

3. **Create a New Branch:**
   Make your changes in a new branch. Ensure your branch name is descriptive and follows a convention such as `feature/`, `bugfix/`, or `enhancement/`.

4. **Release a New Version:**
   If your changes involve releasing a new version of the module, follow the instructions [here](https://www.drupal.org/docs/develop/git/git-for-drupal-project-maintainers/creating-a-project-release).

## Branching Strategy

Learn more about the branching strategy and release naming conventions [here](https://www.drupal.org/docs/develop/git/git-for-drupal-project-maintainers/release-naming-conventions).
